hosc-json
---------

[hosc](http://rohandrape.net/?t=hosc)
<->
{[json](http://www.json.org/) |
[fudi](https://en.wikipedia.org/wiki/FUDI) |
text}

~~~~
Osc Hosc                        Fudi             Json
--- --------------------------- ---------------  ------------------------------------
i   Int32 0                     0                0
h   Int64 0                     0                0
f   Float 0                     0.0              0.0
d   Double 0                    0.0              0.0
s   AsciiString (pack "s")      s                "s"
b   Blob (pack [0,1])           blob: 2 0 1      {"blob":[0,1]}
t   TimeStamp 0                 timestamp: 0.0   {"timestamp":0.0}
m   Midi (MIDI 0 1 2 3)         midi: 0 1 2 3    {"midi":[0,1,2,3]}
    Message "/m" [Int32 0]      /m 0;            ["/m",0]
    Bundle 0 [Message "/m" []]  time 0; /m;      ["#bundle",{"timestamp":0.0},["/m"]]
~~~~

# rationale

Simple text encodings of Osc packets for working with SuperCollider
from environments where binary encoding, decoding or transport are
awkward.

## cli

[json](?t=hosc-json&e=md/json.md)

## variations

The default Json library required is
[microaeson](https://hackage.haskell.org/package/microaeson).
There are also implementations using
[aeson](https://hackage.haskell.org/package/aeson) and
[json](https://hackage.haskell.org/package/json).

© [rd](http://rd.rohandrape.net), 2013-2025, [gpl](http://gnu.org/copyleft/)

* * *

```
$ doctest Sound/Osc/
Examples: 80  Tried: 80  Errors: 0  Failures: 0
$
```
