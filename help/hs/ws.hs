import Sound.Sc3 {- hsc3 -}

-- json-ws.01

ws_01 =
  let kin = in' 1 kr
      o = sinOsc ar (kin 0) 0
  in pan2 o (kin 2) (kin 1)

-- json-ws.02

ws_02 =
  let kin = in' 1 kr
      f = kin 0 * 600 + 400
      a = kin 1 * 0.1
  in sinOsc ar f 0 * a

-- json-ws.03

ws_03 =
  let k0 = control kr "k0" 440
      k1 = control kr "k1" 0
      k2 = control kr "k2" 0.1
      k3 = control kr "k3" 0
  in out k3 (sinOsc ar k0 k1 * k2)

{-
withSC3 (send (n_set (-1) [("k0",660)]))
withSC3 (send (n_set (-1) [("k2",0.01)]))
-}

-- json-ws.04

-- > import Sound.OSC
-- > withSC3 (sendMessage (dumpOSC TextPrinter))

ws_04 =
  let c1 = control kr "c1" 60
      c2 = control kr "c2" 0
      c3 = control kr "c3" (-12)
      c4 = control kr "c4" 0
  in out c4 (sinOsc ar (midiCps c1) c2 * dbAmp c3)
