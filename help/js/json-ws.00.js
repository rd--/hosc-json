$(document).ready(function () {
    var ws = new WebSocket("ws://localhost:9160/");
    $('#message').submit(function (e) {
        var t = $('#text').val();
        var p = $(document.createElement('li')).text(t);
        ws.send(t);
        $('#sent').append(p);
        e.preventDefault();
    });
});
