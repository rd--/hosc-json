window.onload = function () {
    var d = window.document;
    var ws = new WebSocket("ws://localhost:9160/");
    d.onmousemove = function (e) {
        var w_x = window.innerWidth;
        var w_y = window.innerHeight;
        var x = e.clientX / w_x;
        var y = 1 - (e.clientY / w_y);
        var t = JSON.stringify(["/c_set",0,x,1,y]);
        d.getElementById("sent").innerHTML = t;
        ws.send(t);
    };
};
