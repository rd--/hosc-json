window.onload = function () {
    var d = window.document;
    var ws_p = "9160";
    var ws_i = function (p) {
        return new WebSocket("ws://localhost:" + p + "/");
    }
    var ws = ws_i(ws_p);
    d.getElementById("send").onclick = function (e) {
        var t = d.getElementById("text").value;
        var p = d.getElementById("port").value;
        var l = d.createElement("li");
        if (p != ws_p) {
            console.log("reset port:",ws_p,p);
            ws_p = p;
            ws = ws_i(ws_p);
        }
        ws.send(t);
        l.appendChild(d.createTextNode(t));
        d.getElementById("sent").appendChild(l);
        e.preventDefault();
    };
};
