window.onload = function () {

    var st = {
        d : window.document,
        h : "127.0.0.1",
        p : 9160,
        ws : null
    };

    k_recv_evt = function (e) {
        var m = JSON.parse(e.data);
        var t = st.d.getElementById(m[1]).innerHTML = m[2];
        console.log(m);
    };

    k_open_ws = function () {
        if(st.ws !== null) {
            st.ws.close();
        }
        var a = "ws://" + st.h + ":" + st.p + "/";
        console.log("open_ws:",st.h,st.p,a);
        st.ws = new WebSocket(a);
        st.ws.onmessage = k_recv_evt;
    };

    k_init = function (e) {
        st.h = st.d.getElementById("host").value;
        st.p = st.d.getElementById("port").value;
        k_open_ws();
        e.preventDefault();
    };

    st.d.getElementById("init").onclick = k_init;

};
