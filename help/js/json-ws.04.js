window.onload = function () {

    var st = {
        d : window.document,
        t : undefined,
        t_k : undefined,
        t_n : undefined,
        t_active : false,
        t_x : 0,
        t_y : 0,
        ws : new WebSocket("ws://localhost:9160/")
    };

    k_mouse_down = function (e) {
        st.t = e.target;
        st.t_active = true;
        st.t_k = st.t.getAttribute("id");
        st.t_n = parseFloat(st.t.textContent);
        st.t_x = e.clientX;
        st.t_y = e.clientY;
        e.preventDefault();
    };

    k_modify = function (k) {
        if (k.getAttribute("id") !== null) {
            k.onmousedown = k_mouse_down;
        }
    };

    k_install = function () {
        var n = st.d.getElementsByTagName("span");
        for(i = 0; i < n.length; i++) {
            k_modify(n[i]);
        }
    };

    k_install();

    st.d.onmousemove = function (e) {
        if (st.t_active) {
            var x = e.clientX;
            var y = e.clientY;
            var x_d = x - st.t_x;
            var y_d = -(y - st.t_y);
            var d = (Math.abs(x_d) > Math.abs(y_d)) ? x_d : y_d;
            var m = e.ctrlKey ? (e.altKey ? 0.01 : 0.05) : 0.125;
            var n = st.t_n + (d * m);
            var r = ["/n_set",-1,st.t_k,n];
            var t = JSON.stringify(r);
            st.t.textContent = n.toFixed(2);
            st.ws.send(t);
            st.d.getElementById("sent").innerHTML = t;
            console.log(r);
        }
    };

    st.d.onmouseup = function (e) {
        st.t_active = false;
    };

};
