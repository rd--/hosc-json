# hosc-json

## cat

Read [Json](http://json.org/) (JavaScript Object Notation)
representations of [Osc](http://opensoundcontrol.org/) (Open Sound
Control) packets from `stdin`, one per line, and forward them in
binary form to a UDP port (ie. the
[SuperCollider](http://audiosynth.com/) synthesiser daemon `scsynth`).

~~~~
$ echo '["/c_set",0,440]' | hosc-json cat --host=127.0.0.1 --port=57110
~~~~

## cgi

Read a Json Osc packet at the query parameter `j` and forward over Udp
to port specified at `p`.

~~~~
http://localhost/hosc-json-cgi?p=57110&j=["/c_set",0,440,1,0.1]
~~~~

## console

A [readline](http://hackage.haskell.org/package/haskeline) variant of
`hosc-json cat`, supporting history and line editing etc.  It reads
Json Osc packets from `stdin`, one per line, and forwards them over
Udp.

## nrt

Read a [Sc3](http://audiosynth.com/) (SuperCollider 3) Nrt
(non-realtime) score and print the {json|fudi|text} representation of each element to
`stdout`.

An Sc3 Nrt score is simply a sequence of length prefixed OSC bundles.

~~~~
$ hosc-json nrt fudi ~/uc/the-center-is-between-us/salt/osc/fcd/c21.YZ.osc | less
~~~~

## print

A simple text printer, starts a server at the indicated port and
prints all incoming messages in either `fudi` or `json` or plain
`text` format.

~~~~
$ hosc-json print --port=57900 text
~~~~

## ws

A [websocket]( http://www.websocket.org/) server that either prints or translates
between Json-Osc packets at a specified `ws:` port (`--websocket`) and
plain-Osc packets at a Udp port (`--port`).

The direction of operation is given by the first argument: `ws-to-osc` or `osc-to-ws`.

There are test sources ([Chromium](https://www.chromium.org/)
76.0.3809.100) at:

- [01.html](sw/hosc-json/help/html/json-ws.01.html) - plain text
- [02.html](sw/hosc-json/help/html/json-ws.02.html) - mouse coordinates
- [03.svg](sw/hosc-json/help/svg/json-ws.03.svg) - UGen graph (drawing)
- [04.html](sw/hosc-json/help/html/json-ws.04.html) - UGen graph (text)
- [05.html](?t=hosc-json&e=help/html/json-ws.05.html) - text display
