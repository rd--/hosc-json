all:
	echo "hosc-json"

mk-cmd:
	(cd cmd ; make all install)

clean:
	rm -Rf dist dist-newstyle *~
	rm -f Sound/Osc/Type/JSON/*.o
	rm -f Sound/Osc/Type/JSON/*.hi
	(cd cmd ; make clean)

push-all:
	r.gitlab-push.sh hosc-json

push-tags:
	r.gitlab-push.sh hosc-json --tags

indent:
	fourmolu -i Sound

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
