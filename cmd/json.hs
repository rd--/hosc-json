import Control.Monad {- base -}

import Control.Monad.IO.Class {- transformers -}

import qualified Data.ByteString.Char8 as Char8 {- bytestring -}
import qualified Network.WebSockets as Ws {- websockets -}
import qualified System.Console.Haskeline as Line {- haskeline -}

import qualified Music.Theory.List as List {- hmt-base -}
import qualified Music.Theory.Opt as Opt {- hmt-base -}
import qualified Music.Theory.String as String {- hmt-base -}

import qualified Sound.Osc as Osc {- hosc -}
import qualified Sound.Osc.Fd as Osc.Fd {- hosc -}
import qualified Sound.Osc.Transport.Fd.Udp as Osc.Fd {- hosc -}
import qualified Sound.Osc.Text as Osc.Text {- hosc -}

import qualified Sound.Sc3.Server.Nrt as Nrt {- hsc3 -}

import qualified Sound.Osc.Fudi as Fudi {- hosc-json -}
import qualified Sound.Osc.Type.Json.Micro as Json {- hosc-json -}
import qualified Sound.Osc.Transport.Json as Transport.Json {- hosc-json -}

import qualified Www.Minus.Cgi as Cgi {- www-minus -}

-- * Cat

json_cat :: Osc.OscSocketAddress -> IO ()
json_cat address = do
  let send pkt = Osc.withTransport (Osc.openOscSocket address) (Osc.sendPacket pkt)
  forever (Char8.getLine >>= Transport.Json.proc_bytestring_strict send)

-- * Cgi

json_cgi_err :: Show a => a -> IO ()
json_cgi_err e = Cgi.utf8_text_output ("json-cgi: error: " ++ show e)

handle_json :: (Osc.Connection Osc.OscSocket () -> IO ()) -> Json.Value -> IO ()
handle_json withT j =
    case Json.decode_packet j of
      Just o -> do withT (Osc.sendPacket o)
                   Cgi.utf8_text_output ("json-cgi: sent: " ++ show o)
      _ -> json_cgi_err ("json_packet",j)

dispatch_get :: Cgi.Query -> IO ()
dispatch_get q =
    let protocol = read (List.lookup_def "protocol" "Tcp" q)
        hostname = List.lookup_def "hostname" "127.0.0.1" q
        port = read (List.lookup_def "port" "57110" q)
        address = (protocol, hostname, port)
    in case lookup "j" q of
         Just s -> case Json.json_decode_value_str s of
                     Just j -> let t = Osc.withTransport (Osc.openOscSocket address)
                               in handle_json t j
                     _ -> json_cgi_err ("Json.decode",s)
         _ -> json_cgi_err ("Get",q)

json_cgi :: Cgi.Parameters -> IO ()
json_cgi (m,q) =
    case m of
      "GET" -> dispatch_get q
      _ -> json_cgi_err ("json_cgi",m)

-- * Console

json_console :: Osc.OscSocketAddress -> IO ()
json_console address =
  let send pkt = Osc.withTransport (Osc.openOscSocket address) (Osc.sendPacket pkt)
      step = do
        r <- Line.getInputLine "% "
        case r of
          Nothing -> return ()
          Just "quit" -> return ()
          Just s -> liftIO (Transport.Json.proc_string send s) >> step
  in Line.runInputT Line.defaultSettings step

-- * Nrt

-- | Print format for Nrt score
data Fmt = Fudi | Json | Text deriving (Eq,Show,Read,Enum)

-- | Parse format string
fmt_parse :: String -> Fmt
fmt_parse = read . String.capitalise

bundle_pp :: Fmt -> Int -> Osc.BundleOf Osc.Message -> String
bundle_pp fmt k =
  case fmt of
    Fudi -> Fudi.oscBundleToFudi (False, k)
    Json -> Json.json_encode_value_str . Json.encode_bundle
    Text -> Osc.Text.showBundle (Just k)

pp_error :: Fmt -> [(Osc.BundleOf Osc.Message, Osc.BundleOf Osc.Message)] -> IO ()
pp_error fmt err = do
  let hdr = "Error: " ++ show (length err)
      ent = concatMap (\(i,j) -> map (bundle_pp fmt 5) [i,j]) err
  putStrLn (unlines (hdr : ent))

{- | Print Nrt file in indicated format.

> let fn = "/home/rohan/uc/the-center-is-between-us/salt/osc/fcd/c21.YZ.osc"
> let fn = "/home/rohan/uc/invisible/chalk/osc/2019-09-08T14-14-24-b_b.XY.osc"
> let fn = "/tmp/nrt.osc"
> json_nrt Json fn
-}
json_nrt :: Fmt -> FilePath -> IO ()
json_nrt fmt fn = do
  r <- Nrt.readNrt fn
  let wr = putStrLn . bundle_pp fmt 5
  case Nrt.nrt_non_ascending r of
    [] -> return ()
    err -> pp_error fmt err >> error "Non-ascending"
  mapM_ wr (Nrt.nrt_bundles r)

-- * Print

fmt_packet :: Fmt -> Int -> Osc.PacketOf Osc.Message -> String
fmt_packet fmt k =
  case fmt of
    Fudi -> Fudi.oscPacketToFudi (False, k)
    Json -> Json.json_encode_value_str . Json.encode_packet
    Text -> Osc.Text.showPacket (Just k)

json_print :: Fmt -> Int -> IO ()
json_print fmt p = do
  let f fd = forever (Osc.Fd.recvPacket fd >>= putStrLn . fmt_packet fmt 5)
  Osc.Fd.withTransport (Osc.Fd.udp_server p) f

-- * Ws

ws_print :: Ws.ServerApp
ws_print rq = do
  let ws_recv c = do
        d <- Ws.receiveData c
        putStrLn (Char8.unpack d)
  c <- Ws.acceptRequest rq
  forever (ws_recv c)

-- | Read Json-Osc at Ws and write Binary-Osc to Udp.
json_osc_ws_to_osc_udp :: (Osc.PacketOf Osc.Message -> IO ()) -> Ws.ServerApp
json_osc_ws_to_osc_udp osc_send rq = do
  let ws_verbose = False
      ws_recv c = do
        d <- Ws.receiveData c
        when ws_verbose (print d)
        Transport.Json.proc_bytestring_lazy osc_send d
  c <- Ws.acceptRequest rq
  forever (ws_recv c)

-- | Read Binary-Osc at Udp and write Json-Osc to Ws.
osc_udp_to_json_osc_ws :: IO (Osc.PacketOf Osc.Message) -> Ws.PendingConnection -> IO ()
osc_udp_to_json_osc_ws osc_recv rq = do
  let ws_send c = do
        msg <- osc_recv
        let js = Json.json_encode_value (Json.encode_packet msg)
        Ws.sendTextData c js
  c <- Ws.acceptRequest rq
  forever (ws_send c)

-- | Read Binary-Osc at Ws and write Binary-Osc to Udp.
osc_ws_to_osc_udp :: (Osc.PacketOf Osc.Message -> IO ()) -> Ws.ServerApp
osc_ws_to_osc_udp osc_send rq = do
  let ws_verbose = True
      ws_recv c = do
        d <- Ws.receiveData c
        let p = Osc.Fd.decodePacket d
        when ws_verbose (print p)
        osc_send p
  c <- Ws.acceptRequest rq
  forever (ws_recv c)

-- * Main

usage :: [String]
usage =
  ["hosc-json"
  ,""
  ,"  cat [--host] [--port]"
  ,"  console [--host] [--port]"
  ,"  nrt fmt nrt-file"
  ,"  print [--port] fmt"
  ,"  ws print|osc-udp-to-json-osc-ws|json-osc-ws-to-osc-udp [opt]"
  ,""
  ,"    fmt=Fudi|Json|Text"]

opt_def :: [Opt.OptUsr]
opt_def =
  [("protocol","Tcp","string","transport protocol")
  ,("hostname","127.0.0.1","string","host name")
  ,("port","57110","int","port number")
  ,("websocket-port","9160","int","websocket port number")]

main :: IO ()
main = do
  (o,a) <- Opt.opt_get_arg True usage opt_def
  let protocol = Opt.opt_read o "protocol"
      hostname = Opt.opt_get o "hostname"
      port = Opt.opt_read o "port"
      address = (protocol, hostname, port)
      websocket_port = Opt.opt_read o "websocket-port"
      sendUdp pkt = Osc.withTransport (Osc.openOscSocket address) (Osc.sendPacket pkt)
      recv = Osc.withTransport (Osc.openOscSocket address) Osc.recvPacket
  case a of
    ["cat"] -> json_cat address
    ["cgi"] -> Cgi.cgi_main json_cgi
    ["console"] -> json_console address
    ["nrt",fmt,nrt_fn] -> json_nrt (fmt_parse fmt) nrt_fn
    ["print",fmt] -> json_print (fmt_parse fmt) port
    ["ws","json-osc-ws-to-osc-udp"] -> Ws.runServer hostname websocket_port (json_osc_ws_to_osc_udp sendUdp)
    ["ws","osc-udp-to-json-osc-ws"] -> Ws.runServer hostname websocket_port (osc_udp_to_json_osc_ws recv)
    ["ws","osc-ws-to-osc-udp"] -> Ws.runServer hostname websocket_port (osc_ws_to_osc_udp sendUdp)
    ["ws","print"] -> Ws.runServer hostname websocket_port ws_print
    _ -> Opt.opt_usage usage opt_def
