-- | Json / Udp
module Sound.Osc.Transport.Json where

import Control.Monad.IO.Class {- transformers -}

import qualified Data.ByteString {- bytestring -}
import qualified Data.ByteString.Lazy {- bytestring -}
import qualified Data.ByteString.Lazy.UTF8 {- utf8-string -}

import qualified Data.Text {- text -}
import qualified Data.Text.Encoding {- text -}
import qualified Data.Text.Lazy {- text -}
import qualified Data.Text.Lazy.Encoding {- text -}

import qualified Sound.Osc as Osc {- hosc -}

import qualified Sound.Osc.Type.Json.Micro as Json {- hosc-json -}

-- | Decode Json to Osc and run /f/.
proc_json :: MonadIO m => (Osc.PacketOf Osc.Message -> IO ()) -> Json.Value -> m ()
proc_json f j =
  case Json.decode_packet j of
    Just o -> liftIO (f o)
    _ -> error (show ("proc_json", j))

-- | Decode bytes to Json and process.
proc_bytestring_lazy :: MonadIO m => (Osc.PacketOf Osc.Message -> IO ()) -> Data.ByteString.Lazy.ByteString -> m ()
proc_bytestring_lazy withT b =
  case Json.json_decode_value b of
    Just j -> proc_json withT j
    _ -> error (show ("Json.decode", b))

-- | Strict variant.
proc_bytestring_strict :: MonadIO m => (Osc.PacketOf Osc.Message -> IO ()) -> Data.ByteString.ByteString -> m ()
proc_bytestring_strict withT = proc_bytestring_lazy withT . Data.ByteString.Lazy.fromStrict

-- | String variant.
proc_string :: MonadIO m => (Osc.PacketOf Osc.Message -> IO ()) -> String -> m ()
proc_string withT = proc_bytestring_lazy withT . Data.ByteString.Lazy.UTF8.fromString

-- | Strict text variant.
proc_text_strict :: MonadIO m => (Osc.PacketOf Osc.Message -> IO ()) -> Data.Text.Text -> m ()
proc_text_strict withT = proc_bytestring_strict withT . Data.Text.Encoding.encodeUtf8

-- | Lazy text variant.
proc_text_lazy :: MonadIO m => (Osc.PacketOf Osc.Message -> IO ()) -> Data.Text.Lazy.Text -> m ()
proc_text_lazy withT = proc_bytestring_lazy withT . Data.Text.Lazy.Encoding.encodeUtf8
