{- | Fudi <https://en.wikipedia.org/wiki/FUDI> (Pure Data)

This module can print more Osc data than it can read.
The parser only covers the case of plain Osc messages.
-}
module Sound.Osc.Fudi where

import Data.Char {- base -}

import qualified Text.ParserCombinators.Parsec as Parsec {- parsec -}

import qualified Sound.Osc as Osc {- hosc -}
import qualified Sound.Osc.Text as Osc.Text {- hosc -}

-- | A character parser with no user state.
type P a = Parsec.GenParser Char () a

-- | An atom is a string.
type Fudi_Atom = String

-- | A message is a sequence of atoms.
type Fudi_Message = [Fudi_Atom]

-- | Any non-space and non-; character.  Allow escaped space.
atomCharP :: P Char
atomCharP = (Parsec.char '\\' >> Parsec.space) Parsec.<|> Parsec.satisfy (\c -> not (isSpace c) && c /= ';')

-- | Parser for atom.
atomP :: P Fudi_Atom
atomP = Osc.Text.lexemeP (Parsec.many1 atomCharP)

-- | Parser for semicolon lexeme.
semicolonP :: P Char
semicolonP = Osc.Text.lexemeP (Parsec.char ';')

-- | Parser for a message.
messageP :: P Fudi_Message
messageP = Parsec.many1 atomP Osc.Text.>>~ semicolonP

-- | Parser for sequence of messages.
messageSeqP :: P [Fudi_Message]
messageSeqP = Parsec.many1 messageP

{- | Parse Fudi string into a sequence of messages.

>>> p = fudiParse
>>> p "test/blah 123.45314;"
[["test/blah","123.45314"]]

>>> p "my-slider 12;"
[["my-slider","12"]]

>>> p "hello this is a message;"
[["hello","this","is","a","message"]]

>>> p "this message continues\nin the following\nline;"
[["this","message","continues","in","the","following","line"]]

>>> p "you; can; send; multiple messages; in a line;"
[["you"],["can"],["send"],["multiple","messages"],["in","a","line"]]

>>> p "this\\ is\\ one\\ whole\\ atom;"
[["this is one whole atom"]]

>>> p "this_atom_contains_a\\\nnewline_character_in_it;"
[["this_atom_contains_a\nnewline_character_in_it"]]

p "" -- FAIL

p ";" -- FAIL
-}
fudiParse :: String -> [Fudi_Message]
fudiParse = Osc.Text.runP messageSeqP

-- * Osc

-- | Parse float (allow negative literals).
oscFloatP :: P Osc.Datum
oscFloatP = Osc.Text.datumP 'f'

-- | Parse int (allow negative literals).
oscInt32P :: P Osc.Datum
oscInt32P = Osc.Text.datumP 'i'

-- | Blobs are encoded as hash prefixed hex strings.
oscBlobP :: P Osc.Datum
oscBlobP = Parsec.char '#' >> Osc.Text.datumP 'b'

-- | Strings are atoms, they can't include semicolons.
oscStringP :: P Osc.Datum
oscStringP = fmap Osc.string atomP

{- | Parse datum.

>>> let p = Osc.Text.runP oscDatumP
>>> let r = [Osc.int32 (-1), Osc.int32 2, Osc.float (-3.4), Osc.float 5.6, Osc.blob [7], Osc.string "eight"]
>>> map p (words "-1 2 -3.4 5.6 #07 eight") == r
True
-}
oscDatumP :: P Osc.Datum
oscDatumP = Parsec.choice [Parsec.try oscFloatP, oscInt32P, oscBlobP, oscStringP]

-- | Parse message. Do not enforce Osc address requirements (ie. initial /).
oscMessageP :: P Osc.Message
oscMessageP = do
  addr <- atomP
  maybeSig <- Parsec.optionMaybe Osc.Text.oscSignatureP
  dat <- Parsec.many oscDatumP
  _ <- semicolonP
  let validate sig = if Osc.signatureFor dat == sig then return () else error "oscMessage: invalid signature?"
  maybe (return ()) validate maybeSig
  return (Osc.Message addr dat)

-- | Parse message sequence.
oscMessageSeqP :: P [Osc.Message]
oscMessageSeqP = Parsec.many1 oscMessageP

{- | Parse Fudi into an Osc message.  Floats are stored as Float, ints as Int32.

>>> p = fudiParseOsc
>>> p "/p;"
[Message {messageAddress = "/p", messageDatum = []}]

>>> p "/p 1 2.3 txt #0ff0;"
[Message {messageAddress = "/p", messageDatum = [Int32 {d_int32 = 1},Float {d_float = 2.3},AsciiString {d_ascii_string = "txt"},Blob {d_blob = "\SI\240"}]}]

>>> p "/p -1 -2.3 txt #0ff0;"
[Message {messageAddress = "/p", messageDatum = [Int32 {d_int32 = -1},Float {d_float = -2.3},AsciiString {d_ascii_string = "txt"},Blob {d_blob = "\SI\240"}]}]

>>> p "p 1 2.3 txt #0ff0;" -- ALLOW
[Message {messageAddress = "p", messageDatum = [Int32 {d_int32 = 1},Float {d_float = 2.3},AsciiString {d_ascii_string = "txt"},Blob {d_blob = "\SI\240"}]}]

>>> p "/p ,ifsb 1 2.3 txt #0ff0;" -- with signature
[Message {messageAddress = "/p", messageDatum = [Int32 {d_int32 = 1},Float {d_float = 2.3},AsciiString {d_ascii_string = "txt"},Blob {d_blob = "\SI\240"}]}]

>>> p "/p ,ifsb -1 -2.3 txt #0ff0;" -- with signature
[Message {messageAddress = "/p", messageDatum = [Int32 {d_int32 = -1},Float {d_float = -2.3},AsciiString {d_ascii_string = "txt"},Blob {d_blob = "\SI\240"}]}]

>>> p "/c_set 1 0.1;"
[Message {messageAddress = "/c_set", messageDatum = [Int32 {d_int32 = 1},Float {d_float = 0.1}]}]

>>> p "/n_set 100 freq 220;"
[Message {messageAddress = "/n_set", messageDatum = [Int32 {d_int32 = 100},AsciiString {d_ascii_string = "freq"},Int32 {d_int32 = 220}]}]

>>> p "/b_allocRead 0 piano-c5.wav;"
[Message {messageAddress = "/b_allocRead", messageDatum = [Int32 {d_int32 = 0},AsciiString {d_ascii_string = "piano-c5.wav"}]}]

p "" -- FAIL
p ";" -- FAIL
-}
fudiParseOsc :: String -> [Osc.Message]
fudiParseOsc = Osc.Text.runP oscMessageSeqP

{- | Print Osc datum as Fudi string.

>>> oscDatumToFudi 0 (Osc.blob [15, 240])
"#0ff0"
-}
oscDatumToFudi :: Int -> Osc.Datum -> String
oscDatumToFudi fp_prec datum =
  let datumStr = Osc.Text.showDatum (Just fp_prec) datum
  in if Osc.datum_tag datum == 'b' then '#' : datumStr else datumStr

{- | Print an Osc message as a Fudi packet.

>>> let t = ["/p 1 2.3 txt #0ff0;","/p -1 -2.3 txt #0ff0;","#X obj 26 59 midiin;"]
>>> let d = fudiParseOsc (unwords t)
>>> map (oscMessageToFudi (False, 5)) d == t
True

>>> oscMessageToFudi (False, 5) (Osc.Message "/m" [Osc.string "s 1",Osc.string "s\n2"])
"/m s\\ 1 s\\\n2;"
-}
oscMessageToFudi :: (Bool, Int) -> Osc.Message -> String
oscMessageToFudi (withSignature, fp_prec) (Osc.Message a d) =
  let d' = map (oscDatumToFudi fp_prec) d
      d'' = if withSignature then Osc.signatureFor d : d' else d'
  in unwords (a : d'') ++ ";"

{- | Print a sequence of Osc messages as a Fudi packet.

>>> t = ["/p 1 2.3 txt;","/p -1 -2.3 txt;","#X obj 26 59 midiin;"]
>>> oscMessageListToFudi (False, 5) (fudiParseOsc (unwords t)) == unlines t
True

>>> t = ["/p ,ifsb 1 2.3 txt #0ff0;", "/p ,ifsb -1 -2.3 txt #0ff0;", "#X ,siis obj 26 59 midiin;"]
>>> oscMessageListToFudi (True, 5) (fudiParseOsc (unwords t)) == unlines t
True
-}
oscMessageListToFudi :: (Bool, Int) -> [Osc.Message] -> String
oscMessageListToFudi opt = unlines . map (oscMessageToFudi opt)

{- | Print an Osc bundle as a Fudi packet.

>>> oscBundleToFudi (False, 5) (Osc.Bundle 0 [Osc.Message "/m" [Osc.Int32 0]])
"time 0.0;\n/m 0;\n"
-}
oscBundleToFudi :: (Bool, Int) -> Osc.BundleOf Osc.Message -> String
oscBundleToFudi (withSignature, fp_prec) (Osc.Bundle tm msg) =
  let ts = concat ["time ", Osc.Text.showFloatWithPrecision (Just fp_prec) tm, ";\n"]
  in ts ++ oscMessageListToFudi (withSignature, fp_prec) msg

oscPacketToFudi :: (Bool, Int) -> Osc.PacketOf Osc.Message -> String
oscPacketToFudi opt = Osc.at_packet (oscMessageToFudi opt) (oscBundleToFudi opt)

oscPacketListToFudi :: (Bool, Int) -> [Osc.PacketOf Osc.Message] -> String
oscPacketListToFudi opt = unwords . map (oscPacketToFudi opt)
