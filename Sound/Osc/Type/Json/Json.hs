{- | Encoding and decoding of Osc types as Json values.
     This implementation uses the 'json' library,
     which has fewer dependencies than 'aeson'.
-}
module Sound.Osc.Type.Json.Json where

{-
import Data.Ratio {- base -}
import Data.Word {- base -}

import qualified Data.ByteString.Lazy as B {- bytestring -}

import Text.JSON {- json -}

import Sound.Osc.Datum {- hosc -}

import qualified Sound.Osc.Type.Json.Math as Math {- hosc-json -}

-- * Encoders

-- | All 'Integral' values are packed to 'Rational'.
encode_integral :: Integral n => n -> JSValue
encode_integral = JSRational False . toRational

-- | All 'Floating' values are packed to 'Rational'.
encode_floating :: (Real n,Floating n) => n -> JSValue
encode_floating = JSRational False . toRational

-- | Pack 'String'.
encode_string :: String -> JSValue
encode_string = JSString . toJSString

-- | Pack @(key,value)@ pair to 'JSObject'.
encode_assoc :: (String,JSValue) -> JSValue
encode_assoc = JSObject . toJSObject . return

encode_list :: [JSValue] -> JSValue
encode_list = JSArray

-- * Decoding

{- | Decode 'Rational' to 'Datum'.

> m = fromIntegral (maxBound::Data.Int.Int64)
> r = [1,1.5,m,m+1]
> d = [Int64 1,Double 1.5,Int64 9223372036854775807,Double 9.223372036854776e18]
> map rational_to_datum r == d
-}
rational_to_datum :: Rational -> Datum
rational_to_datum n =
    if Math.ratio_is_int n
    then Int64 (fromIntegral (numerator n))
    else Double (fromRational n)

decode_word8 :: JSValue -> Maybe Word8
decode_word8 v =
    case v of
      JSRational _ n -> if Math.ratio_is_word8 n
                        then Just (fromIntegral (numerator n))
                        else Nothing
      _ -> Nothing

decode_datum :: JSValue -> Maybe Datum
decode_datum v =
    case v of
      JSRational _ n -> Just (rational_to_datum n)
      JSString s -> Just (string (fromJSString s))
      JSObject o ->
          case fromJSObject o of
            [("blob",JSArray b)] ->
                mapM decode_word8 b >>= Just . Blob . B.pack
            [("midi",JSArray m)] ->
                case mapM decode_word8 m of
                  Just [p,q,r,s] -> Just (midi (p,q,r,s))
                  _ -> Nothing
            [("timestamp",JSRational _ n)] ->
                Just (TimeStamp (fromRational n))
            _ -> Nothing
      _ -> Nothing
-}
