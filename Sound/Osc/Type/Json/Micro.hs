module Sound.Osc.Type.Json.Micro where

import Data.Bifunctor {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Data.ByteString.Lazy as B {- bytestring -}
import qualified Data.ByteString.Lazy.Char8 as C {- bytestring -}
import qualified Data.Map as Map {- containers -}
import qualified Data.Text as Text {- text -}

import qualified Data.Aeson.Micro as J {- microaeson -}

import qualified Music.Theory.Json as Json {- hmt-base -}
import qualified Music.Theory.Math.Predicate as Math {- hmt-base -}

import Sound.Osc.Datum {- hosc -}
import Sound.Osc.Packet {- hosc -}

-- * Assoc

{- | Pack @(key,value)@ pair as object width single entry.

>>> decode_assoc (encode_assoc ("a",Json.encode_integral 0))
Just ("a",Number 0.0)
-}
encode_assoc :: (String, J.Value) -> J.Value
encode_assoc = J.Object . Map.fromList . return . first Text.pack

decode_assoc :: J.Value -> Maybe (String, J.Value)
decode_assoc j =
  case j of
    J.Object o ->
      case map (first Text.unpack) (Map.toList o) of
        [(k, v)] -> Just (k, v)
        _ -> Nothing
    _ -> Nothing

{- | Decode assoc

>>> decode_assoc_err (encode_assoc ("a",Json.encode_integral 0))
("a",Number 0.0)
-}
decode_assoc_err :: J.Value -> (String, J.Value)
decode_assoc_err = fromMaybe (error "decode_assoc") . decode_assoc

-- * Decode

value_to_midi_err :: J.Value -> MidiData
value_to_midi_err v =
  case v of
    J.Array [m1, m2, m3, m4] -> MidiData (Json.value_to_word8_err m1) (Json.value_to_word8_err m2) (Json.value_to_word8_err m3) (Json.value_to_word8_err m4)
    _ -> error "value_to_midi?"

decode_datum_err :: J.Value -> Datum
decode_datum_err v =
  case v of
    J.Number x -> if Math.double_is_integral x then Int64 (floor x) else Double x
    J.String x -> string (Text.unpack x)
    J.Object x -> case Map.toList x of
      [(k, e)] -> case Text.unpack k of
        "blob" -> Blob (Json.value_to_bytestring_err e)
        "midi" -> Midi (value_to_midi_err e)
        "timestamp" -> TimeStamp (Json.value_to_double_err e)
        _ -> error "value_to_datum?"
      _ -> error "value_to_datum?"
    _ -> error "value_to_datum?"

{- | Json string to Datum.

>>> decode_datum_str "1"
Int64 {d_int64 = 1}

>>> decode_datum_str "1.5"
Double {d_double = 1.5}

>>> decode_datum_str "\"str\""
AsciiString {d_ascii_string = "str"}

>>> decode_datum_str "{\"blob\":[0,1]}"
Blob {d_blob = "\NUL\SOH"}
-}
decode_datum_str :: String -> Datum
decode_datum_str = decode_datum_err . Json.decode_value_err . C.pack

decode_message_err :: J.Value -> Message
decode_message_err v =
  case v of
    J.Array (J.String m : a) -> Message (Text.unpack m) (map decode_datum_err a)
    _ -> error "value_to_message?"

decode_message_str :: String -> Message
decode_message_str = decode_message_err . Json.decode_value_err . C.pack

decode_bundle_err :: J.Value -> BundleOf Message
decode_bundle_err v =
  case v of
    J.Array (J.String m : t : a) -> case (Text.unpack m, decode_datum_err t) of
      ("#bundle", TimeStamp tm) -> Bundle tm (map decode_message_err a)
      _ -> error "value_to_bundle?"
    _ -> error "value_to_bundle?"

-- | Decode 'Packet'.
decode_packet :: J.Value -> Maybe (PacketOf Message)
decode_packet v =
  case v of
    J.Array (J.String m : a) ->
      case Text.unpack m of
        "#bundle" ->
          case uncons a of
            Just (h, t) ->
              case (decode_datum_err h, map decode_message_err t) of
                (TimeStamp tm, msg) -> Just (Packet_Bundle (Bundle tm msg))
                _ -> error "decode_packet"
            Nothing -> error "decode_packet"
        addr -> Just (Packet_Message (Message addr (map decode_datum_err a)))
    _ -> Nothing

-- * Encode

{- | Encode 'O.TimeStamp' data ('O.Time'), ie. the @hosc@ real-valued @NRT@ representation.

>>> encode_timestamp 0.0
Object (fromList [("timestamp",Number 0.0)])
-}
encode_timestamp :: Time -> J.Value
encode_timestamp n = encode_assoc ("timestamp", J.Number n)

-- | Encode 'Blob' data ('B.ByteString').
encode_blob :: B.ByteString -> J.Value
encode_blob b =
  let a = J.Array (map Json.encode_integral (B.unpack b))
  in encode_assoc ("blob", a)

-- | Encode 'Midi' data ('Word8' tuple).
encode_midi :: MidiData -> J.Value
encode_midi (MidiData p q r s) =
  let a = J.Array (map Json.encode_integral [p, q, r, s])
  in encode_assoc ("midi", a)

{- | 'Datum' encoder.  The encoding is shallow, 'Int', 'Float' and 'Double' are all sent to 'J.Number'.
     'Blob', 'TimeStamp' and 'Midi' are tagged.

>>> let enc = json_encode_value_str . encode_datum
>>> enc (int32 0)
"0"

>>> enc (int64 0)
"0"

>>> enc (float 0.0)
"0"

>>> enc (double 0.1)
"0.1"

>>> enc (string "s")
"\"s\""

>>> enc (Blob (B.pack [0, 1]))
"{\"blob\":[0,1]}"

>>> enc (TimeStamp 0)
"{\"timestamp\":0}"

>>> enc (midi (0,1,2,3))
"{\"midi\":[0,1,2,3]}"
-}
encode_datum :: Datum -> J.Value
encode_datum d =
  case d of
    Int32 n -> Json.encode_integral n
    Int64 n -> Json.encode_integral n
    Float n -> Json.encode_floating n
    Double n -> Json.encode_floating n
    AsciiString s -> J.String (Text.pack (ascii_to_string s))
    Blob b -> encode_blob b
    TimeStamp n -> encode_timestamp n
    Midi m -> encode_midi m

{- | 'Message' encoder, the representation is a flat array of @address@ and then arguments.

>>> let m = message "/m" [Int32 0,Float 1,string "s"]
>>> json_encode_value_str (encode_message m)
"[\"/m\",0,1,\"s\"]"

>>> import Sound.Sc3
>>> json_encode_value_str (encode_message (n_free [0]))
"[\"/n_free\",0]"
-}
encode_message :: Message -> J.Value
encode_message (Message a d) = J.Array (J.String (Text.pack a) : map encode_datum d)

{- | 'Bundle' encoder, the representation is a flat array of @#bundle@ tag, 'TimeStamp' and then message arrays.

>>> import Sound.Sc3
>>> let b = bundle 0 [message "/m" []]
>>> json_encode_value_str (encode_bundle b)
"[\"#bundle\",{\"timestamp\":0},[\"/m\"]]"

>>> let b = bundle 0 [c_set1 3 4.5,n_free [0]]
>>> json_encode_value_str (encode_bundle b)
"[\"#bundle\",{\"timestamp\":0},[\"/c_set\",3,4.5],[\"/n_free\",0]]"
-}
encode_bundle :: BundleOf Message -> J.Value
encode_bundle (Bundle t m) = J.Array (J.String (Text.pack "#bundle") : encode_timestamp t : map encode_message m)

-- | 'Packet' encoder.
encode_packet :: PacketOf Message -> J.Value
encode_packet p =
  case p of
    Packet_Message m -> encode_message m
    Packet_Bundle b -> encode_bundle b

-- * Public

-- | Alias for 'J.Value'
type Value = J.Value

-- | Type-specialised 'J.encode'
json_encode_value :: J.Value -> B.ByteString
json_encode_value = J.encode

-- | Encode value to string.
json_encode_value_str :: J.Value -> String
json_encode_value_str = C.unpack . J.encode

-- | Type-specialised and erroring 'J.decode'
json_decode_value :: B.ByteString -> Maybe J.Value
json_decode_value = J.decode

-- | Type-specialised and erroring 'J.decode'
json_decode_value_err :: B.ByteString -> J.Value
json_decode_value_err = fromMaybe (error "json_decode_value") . J.decode

-- | Decode value from string.
json_decode_value_str :: String -> Maybe J.Value
json_decode_value_str = json_decode_value . C.pack
