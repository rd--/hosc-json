-- | Encoding and decoding of Osc types as Json values.
module Sound.Osc.Type.Json.Aeson where

import Data.Bifunctor {- base -}
import Data.Maybe {- base -}
import Data.Word {- base -}

import qualified Data.Aeson as Aeson {- aeson -}
import qualified Data.Aeson.Key as Aeson.Key {- aeson -}
import qualified Data.Aeson.KeyMap as Aeson.KeyMap {- aeson -}
import qualified Data.ByteString.Char8 as ByteString.Char8 {- bytestring -}
import qualified Data.ByteString.Lazy as ByteString {- bytestring -}
import qualified Data.ByteString.Lazy.UTF8 as ByteString.Utf8 {- utf8-string -}

import qualified Data.Text as Text {- text -}
import qualified Data.Vector as Vector {- vector -}

import qualified Sound.Osc.Core as Osc {- hosc -}
import qualified Sound.Osc.Datum as Datum {- hosc -}

import qualified Music.Theory.Math.Predicate as Math {- hmt-base -}

-- | The Json value type.
type Value = Aeson.Value

-- * Error

result_to_value :: Aeson.Result t -> t
result_to_value r =
  case r of
    Aeson.Error err -> error err
    Aeson.Success x -> x

-- | 'error' on 'Aeson.Error'
from_json_err :: Aeson.FromJSON a => String -> Aeson.Value -> a
from_json_err origin o =
  case Aeson.fromJSON o of
    Aeson.Error err -> error (origin ++ ": " ++ err)
    Aeson.Success res -> res

from_json_maybe :: Aeson.FromJSON a => Aeson.Value -> Maybe a
from_json_maybe o =
  case Aeson.fromJSON o of
    Aeson.Error _ -> Nothing
    Aeson.Success res -> Just res

-- * Value

value_to_string :: Aeson.Value -> Maybe String
value_to_string o =
  case o of
    Aeson.String t -> Just (Text.unpack t)
    _ -> Nothing

value_to_string_err :: String -> Aeson.Value -> String
value_to_string_err k = from_json_err ("value_to_string: " ++ k)

value_to_null :: Aeson.Value -> Maybe ()
value_to_null o =
  case o of
    Aeson.Null -> Just ()
    _ -> Nothing

value_to_string_or_null :: Aeson.Value -> Maybe (Either String ())
value_to_string_or_null o =
  case o of
    Aeson.String t -> Just (Left (Text.unpack t))
    Aeson.Null -> Just (Right ())
    _ -> Nothing

value_to_array :: Aeson.Value -> Maybe Aeson.Array
value_to_array o =
  case o of
    Aeson.Array v -> Just v
    _ -> Nothing

value_to_array_err :: Aeson.Value -> Aeson.Array
value_to_array_err = fromMaybe (error "value_to_array") . value_to_array

value_to_object :: Aeson.Value -> Maybe Aeson.Object
value_to_object v =
  case v of
    Aeson.Object o -> Just o
    _ -> Nothing

value_to_object_err :: Aeson.Value -> Aeson.Object
value_to_object_err = fromMaybe (error "value_to_object") . value_to_object

value_to_integer :: Aeson.Value -> Maybe Integer
value_to_integer = from_json_maybe

value_to_integer_err :: Aeson.Value -> Integer
value_to_integer_err = fromMaybe (error "value_to_integer") . value_to_integer

value_to_double :: Aeson.Value -> Maybe Double
value_to_double = from_json_maybe

value_to_double_err :: Aeson.Value -> Double
value_to_double_err = fromMaybe (error "value_to_double") . value_to_double

value_to_int :: Aeson.Value -> Maybe Int
value_to_int = from_json_maybe

value_to_int_err :: Aeson.Value -> Int
value_to_int_err = from_json_err "value_to_int"

value_to_list :: Aeson.Value -> Maybe [Aeson.Value]
value_to_list j =
  case j of
    Aeson.Array a -> Just (Vector.toList a)
    _ -> Nothing

value_to_list_err :: Aeson.Value -> [Aeson.Value]
value_to_list_err = from_json_err "value_to_list"

value_to_object_list_err :: Aeson.Value -> [Aeson.Object]
value_to_object_list_err = from_json_err "value_to_object_list"

{- | Json numbers are 'Either' 'Integer' or 'Double'.

>>> map value_to_number_err [integer_to_value 0,double_to_value 1,double_to_value 2.3]
[Left 0,Left 1,Right 2.3]
-}
value_to_number :: Aeson.Value -> Maybe (Either Integer Double)
value_to_number j =
  case value_to_integer j of
    Just i -> Just (Left i)
    Nothing -> case value_to_double j of
      Just d -> Just (Right d)
      Nothing -> Nothing

value_to_number_err :: Aeson.Value -> Either Integer Double
value_to_number_err = fromMaybe (error "decode_number") . value_to_number

value_to_word8 :: Aeson.Value -> Maybe Word8
value_to_word8 j =
  case value_to_integer j of
    Just i ->
      if i >= 0 && i <= 255
        then Just (fromIntegral i)
        else Nothing
    _ -> Nothing

value_to_word8_err :: Aeson.Value -> Word8
value_to_word8_err = fromMaybe (error "decode_word8?") . value_to_word8

-- * Object

object_lookup :: String -> Aeson.Object -> Maybe Aeson.Value
object_lookup k = Aeson.KeyMap.lookup (Aeson.Key.fromString k)

object_lookup_err :: String -> Aeson.Object -> Aeson.Value
object_lookup_err k o =
  let err = error ("object_lookup: " ++ k ++ " -- " ++ show o)
  in fromMaybe err (object_lookup k o)

object_lookup_str :: String -> Aeson.Object -> Maybe String
object_lookup_str k = fmap (value_to_string_err k) . object_lookup k

object_lookup_str_err :: String -> Aeson.Object -> String
object_lookup_str_err k = value_to_string_err k . object_lookup_err k

object_lookup_str_or_null_err :: String -> Aeson.Object -> Either String ()
object_lookup_str_or_null_err k =
  fromMaybe (error "object_lookup_str_or_null")
    . value_to_string_or_null
    . object_lookup_err k

object_lookup_int :: String -> Aeson.Object -> Maybe Int
object_lookup_int k = maybe Nothing value_to_int . object_lookup k

object_lookup_int_err :: String -> Aeson.Object -> Int
object_lookup_int_err k = value_to_int_err . object_lookup_err k

object_lookup_object_list_err :: String -> Aeson.Object -> [Aeson.Object]
object_lookup_object_list_err k = value_to_object_list_err . object_lookup_err k

object_lookup_object_list_null :: String -> Aeson.Object -> [Aeson.Object]
object_lookup_object_list_null k = maybe [] value_to_object_list_err . object_lookup k

object_lookup_str_list_err :: String -> Aeson.Object -> [String]
object_lookup_str_list_err k = map (value_to_string_err k) . value_to_list_err . object_lookup_err k

object_lookup_str_list_null :: String -> Aeson.Object -> [String]
object_lookup_str_list_null k = maybe [] (map (value_to_string_err k) . value_to_list_err) . object_lookup k

-- * Json-Encode

-- | Type-specialised encode
json_encode_value :: Aeson.Value -> ByteString.ByteString
json_encode_value = Aeson.encode

-- | 'String' variant of 'json_encode_value'.
json_encode_value_str :: Aeson.Value -> String
json_encode_value_str = ByteString.Utf8.toString . json_encode_value

-- * Json-Decode

-- | Type-specialised decode.
json_decode_array :: ByteString.ByteString -> Maybe Aeson.Array
json_decode_array = Aeson.decode

-- | Type-specialised decode.
json_decode_object :: ByteString.ByteString -> Maybe Aeson.Object
json_decode_object = Aeson.decode

json_decode_object_err :: ByteString.ByteString -> Aeson.Object
json_decode_object_err = fromMaybe (error "json_decode_object?") . json_decode_object

-- | Type-specialised decode.
json_decode_value :: ByteString.ByteString -> Maybe Aeson.Value
json_decode_value = Aeson.decode

json_decode_value_err :: ByteString.ByteString -> Aeson.Value
json_decode_value_err = fromMaybe (error "json_decode_value?") . json_decode_value

{- | 'String' variant of 'decode_json'.

>>> let j = json_decode_value_str "[\"/n_set\",-1,\"c1\",66]"
>>> fmap decode_message j
Just (Just (Message {messageAddress = "/n_set", messageDatum = [Int32 {d_int32 = -1},AsciiString {d_ascii_string = "c1"},Int32 {d_int32 = 66}]}))
-}
json_decode_value_str :: String -> Maybe Aeson.Value
json_decode_value_str = json_decode_value . ByteString.Utf8.fromString

-- * Json-Io

-- | Read and decode or error
json_read :: (ByteString.ByteString -> Maybe t) -> FilePath -> IO t
json_read decode_f fn = do
  b <- ByteString.readFile fn
  case decode_f b of
    Nothing -> error ("json_read: decode failed: " ++ fn)
    Just j -> return j

-- | Type-specialised reader
json_read_object :: FilePath -> IO Aeson.Object
json_read_object = json_read json_decode_object

-- | Type-specialised reader
json_read_array :: FilePath -> IO Aeson.Array
json_read_array = json_read json_decode_array

-- * Type-Encode

-- | All 'Integral' values are packed to 'Integer'.
integer_to_value :: Integer -> Aeson.Value
integer_to_value = Aeson.toJSON

-- | All 'Floating' values are packed to 'Double'.
double_to_value :: Double -> Aeson.Value
double_to_value = Aeson.toJSON

-- | Encode 'Number'.
encode_number :: Either Integer Double -> Aeson.Value
encode_number = either integer_to_value double_to_value

-- | Pack 'String'.
string_to_value :: String -> Aeson.Value
string_to_value = Aeson.String . Text.pack

list_to_value :: [Aeson.Value] -> Aeson.Value
list_to_value = Aeson.Array . Vector.fromList

-- * Assoc

{- | Pack @(key,value)@ pair to 'JSObject'.

>>> encode_assoc ("a",integer_to_value 0)
Object (fromList [("a",Number 0.0)])
-}
encode_assoc :: (String, Aeson.Value) -> Aeson.Value
encode_assoc = Aeson.Object . Aeson.KeyMap.fromList . return . first Aeson.Key.fromString

{- | Decode assoc

>>> decode_assoc_err (encode_assoc ("a",integer_to_value 0))
("a",Number 0.0)
-}
decode_assoc :: Aeson.Value -> Maybe (String, Aeson.Value)
decode_assoc j =
  case j of
    Aeson.Object o ->
      case map (first Aeson.Key.toString) (Aeson.KeyMap.toList o) of
        [(k, v)] -> Just (k, v)
        _ -> Nothing
    _ -> Nothing

decode_assoc_err :: Aeson.Value -> (String, Aeson.Value)
decode_assoc_err = fromMaybe (error "decode_assoc") . decode_assoc

-- * Encode

{- | Encode 'Osc.TimeStamp' data ('Osc.Time'), ie. the @hosc@ real-valued
@NRT@ representation.
-}
encode_timestamp :: Osc.Time -> Aeson.Value
encode_timestamp n = encode_assoc ("timestamp", double_to_value n)

encode_integral :: Integral n => n -> Aeson.Value
encode_integral = integer_to_value . fromIntegral

encode_floating :: Real n => n -> Aeson.Value
encode_floating = double_to_value . realToFrac

-- | Encode 'Osc.Blob' data ('ByteString.ByteString').
encode_blob :: ByteString.ByteString -> Aeson.Value
encode_blob b =
  let a = list_to_value (map encode_integral (ByteString.unpack b))
  in encode_assoc ("blob", a)

-- | Encode 'Osc.Midi' data ('Word8' tuple).
encode_midi :: Osc.MidiData -> Aeson.Value
encode_midi (Osc.MidiData p q r s) =
  let a = list_to_value (map encode_integral [p, q, r, s])
  in encode_assoc ("midi", a)

{- | 'Datum' encoder.  The encoding is shallow, 'Osc.Int', 'Osc.Float' and
'Osc.Double' are all sent to 'Aeson.Number'.  'Osc.Blob', 'Osc.TimeStamp' and
'Osc.Midi' are tagged.

>>> json_encode_value_str (encode_datum (Datum.float 0))
"0"

>>> :{
let t = [(Datum.int32 0,"0")
        ,(Datum.int64 0,"0")
        ,(Datum.float 0,"0")
        ,(Datum.double 0.1,"0.1")
        ,(Datum.string "s","\"s\"")
        ,(Datum.Blob (ByteString.pack [0,1]),"{\"blob\":[0,1]}")
        ,(Datum.TimeStamp 0,"{\"timestamp\":0}")
        ,(Datum.midi (0,1,2,3),"{\"midi\":[0,1,2,3]}")]
in map (\(d,s) -> json_encode_value_str (encode_datum d) == s) t
:}
[True,True,True,True,True,True,True,True]
-}
encode_datum :: Osc.Datum -> Aeson.Value
encode_datum d =
  case d of
    Osc.Int32 n -> encode_integral n
    Osc.Int64 n -> encode_integral n
    Osc.Float n -> encode_floating n
    Osc.Double n -> encode_floating n
    Osc.AsciiString s -> string_to_value (ByteString.Char8.unpack s)
    Osc.Blob b -> encode_blob b
    Osc.TimeStamp n -> encode_timestamp n
    Osc.Midi m -> encode_midi m

{- | 'Message' encoder, the representation is a flat array of @address@ and then arguments.

>>> let m = Osc.message "/m" [Datum.Int32 0,Datum.Float 1,Datum.string "s"]
>>> json_encode_value_str (encode_message m)
"[\"/m\",0,1,\"s\"]"

>>> import Sound.Sc3
>>> json_encode_value_str (encode_message (n_free [0]))
"[\"/n_free\",0]"
-}
encode_message :: Osc.Message -> Aeson.Value
encode_message (Osc.Message a d) =
  let a' = string_to_value a
      d' = map encode_datum d
  in list_to_value (a' : d')

{- | 'Osc.Bundle' encoder, the representation is a flat array of @#bundle@ tag,
'Osc.TimeStamp' and then message arrays.

>>> let b = Osc.bundle 0 [Osc.message "/m" []]
>>> json_encode_value_str (encode_bundle b)
"[\"#bundle\",{\"timestamp\":0},[\"/m\"]]"

>>> import Sound.Sc3
>>> let b = Osc.bundle 0 [c_set1 3 4,n_free [0]]
>>> json_encode_value_str (encode_bundle b)
"[\"#bundle\",{\"timestamp\":0},[\"/c_set\",3,4],[\"/n_free\",0]]"
-}
encode_bundle :: Osc.BundleOf Osc.Message -> Aeson.Value
encode_bundle (Osc.Bundle t m) =
  let b = string_to_value "#bundle"
      t' = encode_timestamp t
      m' = map encode_message m
  in list_to_value (b : t' : m')

-- | 'Packet' encoder.
encode_packet :: Osc.PacketOf Osc.Message -> Aeson.Value
encode_packet p =
  case p of
    Osc.Packet_Message m -> encode_message m
    Osc.Packet_Bundle b -> encode_bundle b

-- * Osc Decoding

decode_datum :: Aeson.Value -> Maybe Datum.Datum
decode_datum j =
  case j of
    Aeson.Number _ -> case value_to_number_err j of
      Left n ->
        if Math.in_int32 n
          then Just (Datum.Int32 (fromIntegral n))
          else Just (Datum.Int64 (fromIntegral n))
      Right n ->
        if True
          then Just (Datum.Float (realToFrac n))
          else Just (Datum.Double n)
    Aeson.String t -> Just (Datum.string (Text.unpack t))
    _ -> case decode_assoc j of
      Just ("blob", Aeson.Array v) ->
        mapM value_to_word8 (Vector.toList v) >>= Just . Datum.Blob . ByteString.pack
      Just ("midi", Aeson.Array v) ->
        case mapM value_to_word8 (Vector.toList v) of
          Just [p, q, r, s] -> Just (Datum.midi (p, q, r, s))
          _ -> Nothing
      Just ("timestamp", n) -> Just (Datum.TimeStamp (value_to_double_err n))
      _ -> Nothing

{- | Decode 'Message'.

>>> let m = Osc.message "/m" [Datum.Int32 1,Datum.Float 2.3]
>>> decode_message (encode_message m) == Just m
True
-}
decode_message :: Aeson.Value -> Maybe Osc.Message
decode_message j =
  case value_to_list j of
    Just (m : d) ->
      case decode_datum m of
        Just (Osc.AsciiString m') ->
          mapM decode_datum d
            >>= Just . Osc.message (ByteString.Char8.unpack m')
        _ -> Nothing
    _ -> Nothing

{- | Decode 'Bundle'.

>>> let b = Osc.bundle 0.0 [Osc.message "/m" [Osc.Int32 1,Osc.Float 1.0]]
>>> decode_bundle (encode_bundle b) /= Just b -- float32->int32
True

>>> import Sound.Sc3
>>> let b = Osc.bundle 0 [c_set1 3 3.141,n_free [0]]
>>> let j = encode_bundle b
>>> Just b == decode_bundle j
True
-}
decode_bundle :: Aeson.Value -> Maybe (Osc.BundleOf Osc.Message)
decode_bundle j =
  case value_to_list j of
    Just (b : t : m) ->
      case ( fmap (Osc.ascii_to_string . Osc.d_ascii_string) (decode_datum b)
           , decode_datum t
           ) of
        (Just "#bundle", Just (Osc.TimeStamp t')) ->
          mapM decode_message m >>= Just . Osc.Bundle t'
        _ -> Nothing
    _ -> Nothing

-- | Decode 'Packet'.
decode_packet :: Aeson.Value -> Maybe (Osc.PacketOf Osc.Message)
decode_packet v =
  case decode_bundle v of
    Just b -> Just (Osc.Packet_Bundle b)
    Nothing -> case decode_message v of
      Just m -> Just (Osc.Packet_Message m)
      Nothing -> Nothing
